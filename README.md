**Example code for supervised PCA

Adapted from http://statweb.stanford.edu/~tibs/superpc/tutorial.html


---

## Installation:

   install.packages("superpc")


---

## Usage:

nohup R --no-save < supervised_pca_example.R

